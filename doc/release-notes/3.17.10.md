# RELEASE NOTES 3.17.10 (2024-09-02)

* Limit returned data size for the `transactions` and `blocks` REST endpoints.