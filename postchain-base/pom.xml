<!-- Copyright (c) 2020 ChromaWay AB. See README for license information. -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>postchain</artifactId>
        <groupId>net.postchain</groupId>
        <version>${revision}</version>
    </parent>
    <artifactId>postchain-base</artifactId>
    <packaging>jar</packaging>

    <name>Postchain Base</name>

    <dependencies>
        <!-- Postchain -->
        <dependency>
            <groupId>net.postchain</groupId>
            <artifactId>postchain-common</artifactId>
        </dependency>
        <dependency>
            <groupId>net.postchain</groupId>
            <artifactId>postchain-gtv</artifactId>
        </dependency>
        <dependency>
            <groupId>net.postchain</groupId>
            <artifactId>postchain-gtx-data</artifactId>
        </dependency>
        <dependency>
            <groupId>net.postchain</groupId>
            <artifactId>postchain-spi</artifactId>
        </dependency>
        <dependency>
            <groupId>net.postchain</groupId>
            <artifactId>postchain-admin-service</artifactId>
        </dependency>

        <!-- Kotlin -->
        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-stdlib</artifactId>
        </dependency>
        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-reflect</artifactId>
        </dependency>

        <dependency>
            <groupId>io.github.microutils</groupId>
            <artifactId>kotlin-logging-jvm</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-slf4j-impl</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core-test</artifactId>
            <scope>test</scope>
        </dependency>
        <!-- For Log4j2/YAML configuration -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.dataformat</groupId>
            <artifactId>jackson-dataformat-yaml</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>commons-dbutils</groupId>
            <artifactId>commons-dbutils</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
        </dependency>
        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
        </dependency>

        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-dbcp2</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-configuration2</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-beanutils</groupId>
            <artifactId>commons-beanutils</artifactId>
            <scope>runtime</scope>
            <exclusions>
                <exclusion>
                    <groupId>commons-collections</groupId>
                    <artifactId>commons-collections</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-collections4</artifactId>
        </dependency>

        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
        </dependency>

        <!-- REST -->
        <dependency>
            <groupId>org.http4k</groupId>
            <artifactId>http4k-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.http4k</groupId>
            <artifactId>http4k-format-gson</artifactId>
        </dependency>
        <dependency>
            <groupId>org.http4k</groupId>
            <artifactId>http4k-server-netty</artifactId>
        </dependency>
        <dependency>
            <groupId>org.http4k</groupId>
            <artifactId>http4k-client-apache</artifactId>
        </dependency>
        <dependency>
            <groupId>org.http4k</groupId>
            <artifactId>http4k-ops-micrometer</artifactId>
        </dependency>

        <!-- Netty -->
        <dependency>
            <groupId>io.netty</groupId>
            <artifactId>netty-all</artifactId>
        </dependency>
        <dependency>
            <groupId>io.netty</groupId>
            <artifactId>netty-transport-native-epoll</artifactId>
            <classifier>linux-x86_64</classifier>
        </dependency>

        <dependency>
            <groupId>javax.activation</groupId>
            <artifactId>javax.activation-api</artifactId>
        </dependency>

        <!-- Docker Client -->
        <dependency>
            <groupId>com.github.docker-java</groupId>
            <artifactId>docker-java-core</artifactId>
        </dependency>
        <dependency>
            <groupId>com.github.docker-java</groupId>
            <artifactId>docker-java-transport-zerodep</artifactId>
        </dependency>

        <!-- Metrics -->
        <dependency>
            <groupId>io.micrometer</groupId>
            <artifactId>micrometer-core</artifactId>
        </dependency>
        <dependency>
            <groupId>io.micrometer</groupId>
            <artifactId>micrometer-registry-prometheus</artifactId>
        </dependency>
        <dependency>
            <groupId>io.prometheus</groupId>
            <artifactId>simpleclient_httpserver</artifactId>
        </dependency>

        <!-- Test dependencies -->
        <dependency>
            <groupId>net.postchain</groupId>
            <artifactId>postchain-common</artifactId>
            <type>test-jar</type>
            <scope>test</scope>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>net.postchain</groupId>
            <artifactId>postchain-spi</artifactId>
            <type>test-jar</type>
            <scope>test</scope>
            <version>${project.version}</version>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.testcontainers</groupId>
            <artifactId>testcontainers</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.testcontainers</groupId>
            <artifactId>junit-jupiter</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.testcontainers</groupId>
            <artifactId>postgresql</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>uk.org.webcompere</groupId>
            <artifactId>system-stubs-jupiter</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito.kotlin</groupId>
            <artifactId>mockito-kotlin</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-inline</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.willowtreeapps.assertk</groupId>
            <artifactId>assertk-jvm</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.awaitility</groupId>
            <artifactId>awaitility-kotlin</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- REST test -->
        <dependency>
            <groupId>io.rest-assured</groupId>
            <artifactId>rest-assured</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.skyscreamer</groupId>
            <artifactId>jsonassert</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>test-jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
    <profiles>
        <profile>
            <id>ci</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-failsafe-plugin</artifactId>
                        <configuration>
                            <environmentVariables>
                                <!-- Set our subnode to use the daemon user and group. This will be accepted by initdb
                                since the user exists in the host mounted /etc/passwd file. -->
                                <POSTCHAIN_SUBNODE_USER>1:1</POSTCHAIN_SUBNODE_USER>
                            </environmentVariables>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>win</id>
            <activation>
                <os>
                    <family>Windows</family>
                    <arch>x86</arch>
                </os>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-failsafe-plugin</artifactId>
                        <configuration>
                            <systemPropertyVariables>
                                <DOCKER_HOST_MASTER>172.17.0.1</DOCKER_HOST_MASTER>
                            </systemPropertyVariables>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>macos</id>
            <activation>
                <os>
                    <family>mac</family>
                </os>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-failsafe-plugin</artifactId>
                        <configuration>
                            <systemPropertyVariables>
                                <DOCKER_HOST_MASTER>host.docker.internal</DOCKER_HOST_MASTER>
                                <DOCKER_HOST_SUBNODE>localhost</DOCKER_HOST_SUBNODE>
                            </systemPropertyVariables>
                            <environmentVariables>
                                <!-- Mac users aren't always in /etc/passwd - set it to 1:1 for sub nodes. -->
                                <POSTCHAIN_SUBNODE_USER>1:1</POSTCHAIN_SUBNODE_USER>
                            </environmentVariables>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
