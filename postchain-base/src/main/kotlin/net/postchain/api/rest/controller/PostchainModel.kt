// Copyright (c) 2020 ChromaWay AB. See README for license information.

package net.postchain.api.rest.controller

import io.micrometer.core.instrument.Metrics
import io.micrometer.core.instrument.Timer
import mu.KLogging
import net.postchain.PostchainContext
import net.postchain.api.rest.BlockHeight
import net.postchain.api.rest.BlockSignature
import net.postchain.api.rest.BlockchainNodeState
import net.postchain.api.rest.TransactionsCount
import net.postchain.api.rest.model.ApiRejectedTransaction
import net.postchain.api.rest.model.ApiStatus
import net.postchain.api.rest.model.TxRid
import net.postchain.base.BaseBlockchainContext
import net.postchain.base.ConfirmationProof
import net.postchain.base.configuration.BaseBlockchainConfiguration
import net.postchain.base.configuration.BlockchainConfigurationData
import net.postchain.base.configuration.KEY_SIGNERS
import net.postchain.base.data.BaseBlockWitnessProvider
import net.postchain.base.data.DatabaseAccess
import net.postchain.base.data.DependenciesValidator
import net.postchain.base.withReadConnection
import net.postchain.base.withWriteConnection
import net.postchain.common.BlockchainRid
import net.postchain.common.data.Hash
import net.postchain.common.exception.UserMistake
import net.postchain.common.reflection.newInstanceOf
import net.postchain.concurrent.util.get
import net.postchain.core.BlockRid
import net.postchain.core.BlockchainConfiguration
import net.postchain.core.DefaultBlockchainConfigurationFactory
import net.postchain.core.NODE_ID_AUTO
import net.postchain.core.Storage
import net.postchain.core.TransactionInfoExt
import net.postchain.core.TransactionInfoExtsTruncated
import net.postchain.core.block.BlockDetail
import net.postchain.core.block.BlockDetailsTruncated
import net.postchain.core.block.BlockQueries
import net.postchain.core.block.BlockQueryHeightFilter
import net.postchain.core.block.BlockQueryTimeFilter
import net.postchain.core.block.MultiSigBlockWitnessBuilder
import net.postchain.crypto.PubKey
import net.postchain.crypto.SigMaker
import net.postchain.debug.DiagnosticData
import net.postchain.debug.DiagnosticProperty
import net.postchain.debug.DpBlockchainNodeState
import net.postchain.ebft.rest.contract.StateNodeStatus
import net.postchain.gtv.Gtv
import net.postchain.gtv.GtvArray
import net.postchain.gtv.GtvDictionary
import net.postchain.gtv.GtvEncoder
import net.postchain.gtv.mapper.toObject
import net.postchain.gtx.GTXBlockchainConfigurationFactory
import net.postchain.gtx.GtxQuery
import net.postchain.gtx.UnknownQuery
import net.postchain.logging.BLOCKCHAIN_RID_TAG
import net.postchain.logging.CHAIN_IID_TAG
import net.postchain.logging.FAILURE_RESULT
import net.postchain.logging.QUERY_NAME_TAG
import net.postchain.logging.RESULT_TAG
import net.postchain.logging.SUCCESS_RESULT
import net.postchain.managed.CHAIN0
import net.postchain.managed.config.Chain0BlockchainConfigurationFactory
import net.postchain.managed.config.DappBlockchainConfigurationFactory
import net.postchain.managed.config.ManagedBlockchainConfiguration
import net.postchain.managed.config.ManagedDataSourceAware
import net.postchain.metrics.PostchainModelMetrics
import net.postchain.metrics.QUERIES_METRIC_DESCRIPTION
import net.postchain.metrics.QUERIES_METRIC_NAME
import java.time.Instant

open class PostchainModel(
        val blockchainConfiguration: BlockchainConfiguration,
        val blockQueries: BlockQueries,
        final override val blockchainRid: BlockchainRid,
        val storage: Storage,
        val postchainContext: PostchainContext,
        private val diagnosticData: DiagnosticData,
        override val queryCacheTtlSeconds: Long
) : Model {

    companion object : KLogging()

    final override val chainIID = blockchainConfiguration.chainID
    protected val metrics = PostchainModelMetrics(chainIID, blockchainRid)

    private val currentRawConfiguration = GtvEncoder.encodeGtv(blockchainConfiguration.rawConfig)

    override var live = true

    override fun postTransaction(tx: ByteArray): Unit = throw NotSupported("Posting a transaction to this blockchain on this node is not supported")

    override fun getTransaction(txRID: TxRid): ByteArray? = blockQueries.getTransactionRawData(txRID.bytes).get()

    override fun getTransactionInfo(txRID: TxRid, includeTxData: Boolean): TransactionInfoExt? =
            blockQueries.getTransactionInfo(txRID.bytes, includeTxData).get()

    override fun getTransactionsInfo(timeFilter: BlockQueryTimeFilter, limit: Int, maxDataSize: Int): TransactionInfoExtsTruncated =
            blockQueries.getTransactionsInfo(timeFilter, limit, maxDataSize).get()

    override fun getTransactionsInfoBySigner(timeFilter: BlockQueryTimeFilter, limit: Int, signer: PubKey, maxDataSize: Int): TransactionInfoExtsTruncated =
            blockQueries.getTransactionsInfoBySigner(timeFilter, limit, signer, maxDataSize).get()


    override fun getLastTransactionNumber(): TransactionsCount =
            TransactionsCount(blockQueries.getLastTransactionNumber().get())

    override fun getBlocksBetweenTimes(timeFilter: BlockQueryTimeFilter, limit: Int, txHashesOnly: Boolean, maxDataSize: Int, excludeEmpty: Boolean): BlockDetailsTruncated =
            blockQueries.getBlocksBetweenTimes(timeFilter, limit, txHashesOnly, maxDataSize, excludeEmpty).get()

    override fun getBlocksBetweenHeights(heightFilter: BlockQueryHeightFilter, limit: Int, txHashesOnly: Boolean, maxDataSize: Int, excludeEmpty: Boolean): BlockDetailsTruncated =
            blockQueries.getBlocksBetweenHeights(heightFilter, limit, txHashesOnly, maxDataSize, excludeEmpty).get()

    override fun getBlock(blockRID: BlockRid, txHashesOnly: Boolean): BlockDetail? =
            blockQueries.getBlock(blockRID.data, txHashesOnly).get()

    override fun getBlock(height: Long, txHashesOnly: Boolean): BlockDetail? {
        val blockRid = blockQueries.getBlockRid(height).get()
        return blockRid?.let { getBlock(BlockRid(it), txHashesOnly) }
    }

    override fun confirmBlock(blockRID: BlockRid): BlockSignature? {
        return blockQueries.getBlock(blockRID.data, true).get()?.let {
            val blockSigMaker = when (blockchainConfiguration) {
                is BaseBlockchainConfiguration -> blockchainConfiguration.blockSigMaker
                is ManagedBlockchainConfiguration -> blockchainConfiguration.configuration.blockSigMaker
                else -> throw UserMistake("Unknown blockchain configuration detected: " + blockchainConfiguration.javaClass.simpleName)
            }
            val witnessProvider = BaseBlockWitnessProvider(
                    postchainContext.cryptoSystem,
                    blockSigMaker,
                    blockchainConfiguration.signers.toTypedArray()
            )
            val witnessBuilder = witnessProvider.createWitnessBuilderWithOwnSignature(blockRID) as MultiSigBlockWitnessBuilder
            BlockSignature.fromSignature(witnessBuilder.getMySignature())
        }
    }

    override fun getConfirmationProof(txRID: TxRid): ConfirmationProof? =
            blockQueries.getConfirmationProof(txRID.bytes).get()

    override fun getStatus(txRID: TxRid): ApiStatus =
            throw NotSupported("Checking transaction status is not supported for this blockchain on this node")

    override fun getWaitingTransactions(): List<TxRid> =
            throw NotSupported("Fetching waiting transactions is not supported for this blockchain on this node")

    override fun getWaitingTransaction(txRID: TxRid): Pair<ByteArray, Instant>? =
            throw NotSupported("Fetching waiting transaction is not supported for this blockchain on this node")

    override fun getRejectedTransactions(): List<ApiRejectedTransaction> =
            throw NotSupported("Fetching rejected transactions is not supported for this blockchain on this node")

    override fun query(query: GtxQuery): Gtv {
        val timerBuilder = Timer.builder(QUERIES_METRIC_NAME)
                .description(QUERIES_METRIC_DESCRIPTION)
                .tag(CHAIN_IID_TAG, chainIID.toString())
                .tag(BLOCKCHAIN_RID_TAG, blockchainRid.toHex())
                .tag(QUERY_NAME_TAG, query.name)
        val sample = Timer.start(Metrics.globalRegistry)
        return try {
            val result = blockQueries.query(query.name, query.args).get()
            sample.stop(timerBuilder
                    .tag(RESULT_TAG, SUCCESS_RESULT)
                    .register(Metrics.globalRegistry))
            result
        } catch (e: UnknownQuery) {
            // do not add metrics for unknown queries to avoid blowing up QUERY_NAME_TAG dimension
            throw e
        } catch (e: Exception) {
            sample.stop(timerBuilder
                    .tag(RESULT_TAG, FAILURE_RESULT)
                    .register(Metrics.globalRegistry))
            throw e
        }
    }

    override fun nodeStatusQuery(): StateNodeStatus =
            diagnosticData[DiagnosticProperty.BLOCKCHAIN_NODE_STATUS]?.value as? StateNodeStatus
                    ?: throw NotFoundError("NotFound")

    @Suppress("UNCHECKED_CAST")
    override fun nodePeersStatusQuery(): List<StateNodeStatus> =
            diagnosticData[DiagnosticProperty.BLOCKCHAIN_NODE_PEERS_STATUSES]?.value as? List<StateNodeStatus>
                    ?: throw NotFoundError("NotFound")

    override fun getCurrentBlockHeight(): BlockHeight = BlockHeight(blockQueries.getLastBlockHeight().get() + 1)

    override fun getBlockchainNodeState(): BlockchainNodeState {
        val nodeState = diagnosticData[DiagnosticProperty.BLOCKCHAIN_NODE_STATE]?.value as? DpBlockchainNodeState
                ?: throw NotFoundError("NotFound")
        return BlockchainNodeState(nodeState.name)
    }

    override fun getBlockchainConfiguration(height: Long): ByteArray? = withReadConnection(storage, chainIID) { ctx ->
        if (height < 0) {
            currentRawConfiguration
        } else {
            postchainContext.configurationProvider.getHistoricConfiguration(ctx, chainIID, height)
        }
    }

    override fun validateBlockchainConfiguration(configuration: Gtv) {
        val fixedConfiguration = if (configuration[KEY_SIGNERS] == null) {
            GtvDictionary.build(configuration.asDict() + (KEY_SIGNERS to GtvArray(emptyArray())))
        } else {
            configuration
        }
        val blockConfData = fixedConfiguration.toObject<BlockchainConfigurationData>()
        withWriteConnection(storage, chainIID) { eContext ->
            val blockchainRid = DatabaseAccess.of(eContext).getBlockchainRid(eContext)!!
            val partialContext = BaseBlockchainContext(chainIID, blockchainRid, NODE_ID_AUTO, postchainContext.appConfig.pubKeyByteArray)
            val factory = if (blockchainConfiguration is ManagedDataSourceAware) {
                val factory = newInstanceOf<GTXBlockchainConfigurationFactory>(blockConfData.configurationFactory)
                if (chainIID == CHAIN0) {
                    Chain0BlockchainConfigurationFactory(factory, postchainContext.appConfig, storage)
                } else {
                    DappBlockchainConfigurationFactory(factory, blockchainConfiguration.dataSource)
                }
            } else {
                DefaultBlockchainConfigurationFactory().supply(blockConfData.configurationFactory)
            }

            val blockSigMaker: SigMaker = object : SigMaker {
                override fun signMessage(msg: ByteArray) = throw NotImplementedError("SigMaker")
                override fun signDigest(digest: Hash) = throw NotImplementedError("SigMaker")
            }
            val config = factory.makeBlockchainConfiguration(blockConfData, partialContext, blockSigMaker, eContext, postchainContext.cryptoSystem)
            DependenciesValidator.validateBlockchainRids(eContext, config.blockchainDependencies)
            config.initializeModules(postchainContext)
            GTXBlockchainConfigurationFactory.extraConfigurationValidation(blockConfData, eContext)

            false
        }
    }

    override fun getNextBlockchainConfigurationHeight(height: Long): BlockHeight? = withReadConnection(storage, chainIID) { ctx ->
        DatabaseAccess.of(ctx).findNextConfigurationHeight(ctx, height)?.let { BlockHeight(it) }
    }

    override fun toString(): String = "${this.javaClass.simpleName}(chainId=$chainIID)"
}
