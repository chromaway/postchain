// Copyright (c) 2020 ChromaWay AB. See README for license information.

package net.postchain.containers.infra

import net.postchain.PostchainContext
import net.postchain.api.rest.infra.RestApiConfig
import net.postchain.common.exception.UserMistake
import net.postchain.config.app.AppConfig
import net.postchain.config.blockchain.BlockchainConfigurationProvider
import net.postchain.containers.api.DefaultMasterApiInfra
import net.postchain.containers.bpm.ContainerEnvironment
import net.postchain.containers.bpm.ContainerManagedBlockchainProcessManager
import net.postchain.core.BlockchainInfrastructure
import net.postchain.core.BlockchainProcessManager
import net.postchain.managed.ManagedEBFTInfrastructureFactory
import net.postchain.network.mastersub.master.DefaultMasterConnectionManager

open class MasterManagedEbftInfraFactory : ManagedEBFTInfrastructureFactory() {

    companion object {
        fun validateSubnodeUser(subnodeUser: String?) {
            if (subnodeUser == null) {
                throw UserMistake("POSTCHAIN_SUBNODE_USER must be specified")
            } else {
                val values = subnodeUser.split(":")
                if (!values.all { it.matches("\\d+".toRegex()) }) {
                    throw UserMistake("POSTCHAIN_SUBNODE_USER requires format <uid> or <uid>:<gid>")
                }
                if (values.contains("0")) {
                    throw UserMistake("POSTCHAIN_SUBNODE_USER can't be set to root")
                }
            }
        }
    }

    override fun makeBlockchainInfrastructure(postchainContext: PostchainContext): BlockchainInfrastructure {
        with(postchainContext) {
            ContainerEnvironment.init(appConfig)
            val containerNodeConfig = loadContainerNodeConfig(appConfig)
            val restApiConfig = RestApiConfig.fromAppConfig(appConfig)
            val connectionManager = DefaultMasterConnectionManager(appConfig, containerNodeConfig, postchainContext.blockQueriesProvider)
            val syncInfra = DefaultMasterSyncInfra(this, connectionManager, containerNodeConfig)
            val apiInfra = DefaultMasterApiInfra(restApiConfig, nodeDiagnosticContext, postchainContext)

            return DefaultMasterBlockchainInfra(this, syncInfra, apiInfra)
        }
    }

    override fun makeProcessManager(
            postchainContext: PostchainContext,
            blockchainInfrastructure: BlockchainInfrastructure,
            blockchainConfigurationProvider: BlockchainConfigurationProvider,
    ): BlockchainProcessManager {
        val blockchainProcessManager = ContainerManagedBlockchainProcessManager(
                postchainContext,
                blockchainInfrastructure as MasterBlockchainInfra,
                blockchainConfigurationProvider,
                getProcessManagerExtensions(postchainContext, blockchainInfrastructure)
        )
        (blockchainInfrastructure as DefaultMasterBlockchainInfra).registerAfterSubnodeCommitListener(blockchainProcessManager)

        return blockchainProcessManager
    }

    fun loadContainerNodeConfig(appConfig: AppConfig): ContainerNodeConfig {
        val config = ContainerNodeConfig.fromAppConfig(appConfig)
        validateSubnodeUser(config.subnodeUser)

        return config
    }
}