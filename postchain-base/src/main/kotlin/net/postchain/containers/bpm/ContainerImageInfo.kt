package net.postchain.containers.bpm

data class ContainerImageInfo(val name: String, val url: String, val digest: String)
