package net.postchain.containers.bpm.fs

data class ResourceLimitsInfo(val spaceUsedMiB: Long, val spaceHardLimitMiB: Long)