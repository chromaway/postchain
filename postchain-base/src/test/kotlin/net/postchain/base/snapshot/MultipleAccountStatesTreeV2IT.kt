package net.postchain.base.snapshot

class MultipleAccountStatesTreeV2IT : MultipleAccountStatesTreeIT() {
    override val protocolVersion: Int = 2
}