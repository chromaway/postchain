package net.postchain.base.snapshot

class TwoAccountsStateTreeV2IT : TwoAccountsStateTreeIT() {
    override val protocolVersion: Int = 2
}