package net.postchain.base.snapshot

class EmptyAccountStateTreeV2IT : EmptyAccountStateTreeIT() {
    override val protocolVersion: Int = 2
}