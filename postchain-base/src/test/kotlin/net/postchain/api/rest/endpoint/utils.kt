package net.postchain.api.rest.endpoint

import net.postchain.crypto.Secp256K1CryptoSystem

val cryptoSystem = Secp256K1CryptoSystem()
