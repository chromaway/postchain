package net.postchain.base.configuration

@Suppress("EnumEntryName")
enum class BlockchainFeatures {
    merkle_hash_version
}
