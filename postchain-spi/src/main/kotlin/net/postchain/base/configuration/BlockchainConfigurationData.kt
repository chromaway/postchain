package net.postchain.base.configuration

import net.postchain.base.BaseDependencyFactory
import net.postchain.common.BlockchainRid
import net.postchain.common.data.Hash
import net.postchain.gtv.Gtv
import net.postchain.gtv.GtvFactory
import net.postchain.gtv.mapper.DefaultEmpty
import net.postchain.gtv.mapper.DefaultValue
import net.postchain.gtv.mapper.Name
import net.postchain.gtv.mapper.Nested
import net.postchain.gtv.mapper.Nullable
import net.postchain.gtv.mapper.RawGtv
import net.postchain.gtv.mapper.toObject
import net.postchain.gtv.merkle.GtvMerkleHashCalculatorBase
import net.postchain.gtv.merkle.makeMerkleHashCalculator
import net.postchain.gtv.merkleHash

data class BlockchainConfigurationData(
        @RawGtv
        val rawConfig: Gtv,

        @Name(KEY_SIGNERS)
        val signers: List<ByteArray>,
        @Name(KEY_SYNC)
        @Nullable
        val synchronizationInfrastructure: String?,
        @Name(KEY_SYNC_EXT)
        @Nullable
        val synchronizationInfrastructureExtension: List<String>?,
        @Name(KEY_CONFIGURATIONFACTORY)
        val configurationFactory: String,
        /**
         * NB: The default value is set so that the TX queue will fill up fast, b/c the client should display this
         * info to the user (spinning ball etc) so that the client understands that the system is down.
         * Alex spoke about making TX resend automatic, after a pause, when 503 error is returned, so that no action
         * from the user's side has to be taken to eventually get the TX into the queue.
         */
        @Name(KEY_QUEUE_CAPACITY)
        @DefaultValue(defaultLong = 2500) // 5 seconds (if 500 tps)
        val txQueueSize: Long,
        @Name(KEY_QUEUE_TX_RECHECK_INTERVAL)
        @DefaultValue(defaultLong = 5 * 60 * 1000) // 5 minutes
        val txQueueRecheckInterval: Long,

        @Name(KEY_BLOCKSTRATEGY_NAME)
        @Nested(KEY_BLOCKSTRATEGY)
        @DefaultValue(defaultString = "net.postchain.base.BaseBlockBuildingStrategy")
        val blockStrategyName: String,
        @Name(KEY_BLOCKSTRATEGY)
        @Nullable
        val blockStrategy: Gtv?,
        @Name(KEY_HISTORIC_BRID)
        @Nullable
        private val historicBridAsByteArray: ByteArray?,
        @Name(KEY_DEPENDENCIES)
        @Nullable
        private val blockchainDependenciesRaw: Gtv?,
        @Name(KEY_GTX)
        @Nullable
        val gtx: Gtv?,
        @Name(KEY_CONFIG_CONSENSUS_STRATEGY)
        @Nullable
        private val configConsensusStrategyString: String?,
        @Name(KEY_QUERY_CACHE_TTL_SECONDS)
        @DefaultValue(defaultLong = 0)
        val queryCacheTtlSeconds: Long?,
        @Name(KEY_MAX_BLOCK_FUTURE_TIME)
        @DefaultValue(defaultLong = 60 * 1000) // 1 minute
        val maxBlockFutureTime: Long,
        @Name(KEY_ADD_PRIMARY_KEY_TO_HEADER)
        @DefaultValue(defaultBoolean = false)
        val addPrimaryKeyToHeader: Boolean,
        @Name(KEY_FEATURES)
        @DefaultEmpty
        val features: Map<String, Gtv>,
) {
    val historicBrid = historicBridAsByteArray?.let { BlockchainRid(it) }
    val blockchainDependencies = blockchainDependenciesRaw?.let { BaseDependencyFactory.build(it) } ?: listOf()
    val configConsensusStrategy = configConsensusStrategyString?.let { ConfigConsensusStrategy.valueOf(it) }
    val merkleHashVersion: Long = features[BlockchainFeatures.merkle_hash_version.name]?.asInteger() ?: 1L
    val merkleHashCalculator by lazy {
        makeMerkleHashCalculator(merkleHashVersion)
    }
    val configHash by lazy {
        rawConfig.merkleHash(merkleHashCalculator)
    }

    companion object {
        @JvmStatic
        fun fromRaw(
                rawConfigurationData: ByteArray): BlockchainConfigurationData =
                GtvFactory.decodeGtv(rawConfigurationData).toObject()

        @JvmStatic
        fun merkleHash(configuration: Gtv): Hash {
            return configuration.merkleHash(merkleHashCalculator(configuration))
        }

        @JvmStatic
        fun merkleHashCalculator(configuration: Gtv): GtvMerkleHashCalculatorBase {
            return makeMerkleHashCalculator(merkleHashVersion(configuration))
        }

        @JvmStatic
        fun merkleHashVersion(configuration: Gtv): Long {
            val features = configuration[KEY_FEATURES]?.asDict()
            return features?.get(BlockchainFeatures.merkle_hash_version.name)?.asInteger() ?: 1L
        }
    }
}
