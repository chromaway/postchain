// Copyright (c) 2020 ChromaWay AB. See README for license information.

package net.postchain.core

import net.postchain.common.exception.TransactionIncorrect
import net.postchain.common.exception.UserMistake
import net.postchain.common.tx.EnqueueTransactionResult
import net.postchain.common.tx.TransactionStatus
import net.postchain.common.types.WrappedByteArray
import java.time.Instant
import kotlin.time.Duration

/**
 * Transactor is an individual operation which can be applied to the database
 * Transaction might consist of one or more operations
 * Transaction should be serializable, but transactor doesn't need to have a serialized
 * representation as we only care about storing of the whole Transaction
 */
interface Transactor {
    // special transactions cannot be added to a transaction queue,
    // they can only be appended directly by blockchain engine
    fun isSpecial(): Boolean

    /**
     * Check if correct given that transactor is applied during syncing.
     * This check should be less restrictive than `checkCorrectness`.
     *
     * @return if correct
     * @throws UserMistake if not correct
     */
    fun checkCorrectnessWhileSyncing() = checkCorrectness()

    /**
     * Check if correct.
     * @return if correct
     * @throws UserMistake if not correct */
    fun checkCorrectness() {
        @Suppress("DEPRECATION")
        if (!isCorrect()) {
            throw UserMistake("Transactor is not correct")
        }
    }

    @Deprecated(message = "Use checkCorrectness() instead to be able to get error message")
    fun isCorrect(): Boolean = throw NotImplementedError("isCorrect() is no longer supported, use checkCorrectness() instead")

    fun apply(ctx: TxEContext): Boolean

    fun applyWhileSyncing(ctx: TxEContext): Boolean = apply(ctx)
}

interface Transaction : Transactor {
    fun getRawData(): ByteArray
    fun getRID(): ByteArray  // transaction unique identifier which is used as a reference to it
    fun getHash(): ByteArray // hash of transaction content

    override fun checkCorrectness() {
        @Suppress("DEPRECATION")
        if (!isCorrect()) {
            throw TransactionIncorrect(getRID())
        }
    }
}

interface SignableTransaction : Transaction {
    val signers: Array<ByteArray>
}

interface TransactionFactory {
    fun decodeTransaction(data: ByteArray): Transaction
    fun decodeAndValidateTransaction(data: ByteArray): Transaction
}

data class RejectedTransaction(val txRID: WrappedByteArray, val reason: Exception, val timestamp: Instant)

interface TransactionQueue {
    /**
     * Take a transaction from queue without waiting.
     *
     * @return the next transaction in queue, or `null` if queue is empty
     */
    fun takeTransaction(): Transaction?

    /**
     * Take a transaction from queue, wait up to `timeout` if queue is initially empty.
     *
     * @return the next transaction in queue, or `null` if queue is still empty after waiting
     */
    fun takeTransaction(timeout: Duration): Transaction?

    fun enqueue(tx: Transaction): EnqueueTransactionResult
    fun findTransaction(txRID: WrappedByteArray): Transaction?
    fun getTransactionStatus(txRID: ByteArray): TransactionStatus
    fun getTransactionQueueSize(): Int
    fun removeAll(transactionsToRemove: Collection<Transaction>)
    fun rejectTransaction(tx: Transaction, reason: Exception, timestamp: Instant? = null)
    fun getRejectionReason(txRID: WrappedByteArray): Pair<Exception, Instant>?
    fun retryAllTakenTransactions()
    fun flushTransaction(tx: Transaction)
    fun takenTransactions(): List<Transaction>
    fun waitingTransactions(): List<Transaction>
    fun waitingTransaction(txRID: WrappedByteArray): Pair<ByteArray, Instant>?
    fun rejectedTransactions(): List<RejectedTransaction>
}
