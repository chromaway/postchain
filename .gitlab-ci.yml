image: registry.gitlab.com/chromaway/core-tools/chromia-images/maven-docker-java21:1.0.5@sha256:263fc66ab6ed7b3ee9afbd3726c6ea65dc16e186d41fee2b1844bcad71f7f864
include:
  - project: "chromaway/core-infra/gitlab-automation"
    ref: 1.2.0
    file:
      - templates/release.yml
      - templates/maven-dependency-scanning.yml
      - templates/report-code-coverage.yml

.setup:
  tags:
    - saas-linux-large-amd64
  before_script:
    - docker run
      --detach
      --name postgres
      --env POSTGRES_PASSWORD=postchain
      --env POSTGRES_USER=postchain
      --env PGDATA=/pgtmpfs
      --tmpfs=/pgtmpfs:size=1000m
      --publish 5432:5432
      postgres:16.7-alpine3.21@sha256:97a14a17b1fea5ae1ab33024ca556bb4fedc8709bea5722cb8b7665a9cabb656
  # Necessary to be able to launch PostgreSQL in Docker manually. PostgreSQL is
  # launched in Docker manually (and not as a service, here) because then we can
  # use a tmpfs (in-memory disk) for the data, making it faster.
  services:
    - name: docker:23.0.4-dind
      alias: docker
      command: [ "--tls=false" , "--experimental" ]
  artifacts:
    when: always
    paths:
      - "**/*.log"
      - $JACOCO_REPORT_DIR/*
    reports:
      junit:
        - "*/target/surefire-reports/TEST-*.xml"
        - "*/target/failsafe-reports/TEST-*.xml"
    expire_in: 1 week

stages:
  - build
  - code-coverage
  - slow-integration-tests
  - deploy
  - dependency-check
  - prepare_release
  - release

# Triggered by that something was merged into or pushed onto main
deploy-snapshot:
  extends: .setup
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == "dev"
  script:
    - CURRENT_VERSION=$(git describe --tags --abbrev=0)
    - IFS=. read -r MAJOR MINOR PATCH <<< "$CURRENT_VERSION"
    - SNAPSHOT_VERSION=$MAJOR.$((MINOR + 1)).0-SNAPSHOT
    - mvn
      $MAVEN_CLI_OPTS
      --activate-profiles ci,slow-it
      -Drevision=$SNAPSHOT_VERSION
      clean source:jar deploy

# Triggered by that a semver tag was pushed
deploy:
  extends: .setup
  stage: deploy
  rules:
    - if: $CI_COMMIT_TAG =~ /^[0-9]+\.[0-9]+\.[0-9]+$/
  script:
    - mvn
      $MAVEN_CLI_OPTS
      --activate-profiles ci,slow-it
      -Drevision=$CI_COMMIT_TAG
      clean source:jar deploy

prepare_release:
  stage: prepare_release
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v?\d+\.\d+\.\d+$/'
  script:
    - 'curl -H "PRIVATE-TOKEN: $TMP_CI_API_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/changelog?version=$CI_COMMIT_TAG" | jq -r .notes > release_notes.md'
  artifacts:
    paths:
      - release_notes.md


gitlab-release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: prepare_release
      artifacts: true
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v?\d+\.\d+\.\d+$/'
  script:
    - echo "Running the release job."
  release:
    name: 'Postchain $CI_COMMIT_TAG'
    description: release_notes.md
    tag_name: '$CI_COMMIT_TAG'

build:
  extends:
    - .setup
    - .visualise-test-coverage
  stage: build
  interruptible: true
  except:
    refs:
      - dev
      - tags
  script:
    - mvn
      $MAVEN_CLI_OPTS
      --activate-profiles ci,coverage
      verify

test-coverage-report:
  stage: code-coverage
  except:
    refs:
      - dev
      - tags
  dependencies:
    - build

slow-integration-tests:
  extends: .setup
  stage: slow-integration-tests
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "dev"
      when: always
    - if: $CI_PIPELINE_SOURCE == "push"
      when: manual
      allow_failure: true
  script:
    - mvn
      $MAVEN_CLI_OPTS
      --activate-profiles ci,slow-it
      clean verify

dependency-check:
  extends: .maven-dependency-check
  stage: dependency-check
  interruptible: true
  only:
    variables:
      - $RUN_DEPENDENCY_CHECK == "true"

variables:
  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: "-DexcludedGroups=no-ci --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true -s .gitlab-settings.xml"
  MAVEN_CLI_NVD_OPTS: "-DnvdDatafeedUrl=https://nvd-data-feeds.chromia.dev/nvdcve-{0}.json.gz"
  POSTCHAIN_DB_URL: jdbc:postgresql://docker/postchain
  DOCKER_HOST: tcp://docker:2375
  DOCKER_TLS_CERTDIR: ""
  DOCKER_DRIVER: overlay2
  DOCKER_CLI_EXPERIMENTAL: enabled
  TEST_MOUNT_DIRECTORY: /builds/$CI_PROJECT_PATH/mnt
  JACOCO_REPORT_DIR: postchain-coverage-report-aggregate/target/site/jacoco-aggregate

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches add 'key: "$CI_JOB_NAME"'
cache:
  paths:
    - .m2/repository
